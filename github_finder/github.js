class GitHub {
  constructor() { // curl cliendId: client secret htts....
    this.client_id = 'ed1027db5a6bf64b4751';
    this.client_secret = 'd45c54fe37db977bff52ffbbd6e4800127090aab';
    this.repos_count = 5;
    this.repos_sort = 'created: asc';
  }

  async getUser(user) {
    let id_secret_pair = this.client_id + ':' + this.client_secret;
    let encoded64 = window.btoa(id_secret_pair);
    const profileResponse = await fetch(`https://api.github.com/users/${user}`, {
      headers: {
        Authorization: "Basic " + encoded64
      }
    });
    // // fetch repos

    const reposResponse = await fetch(`https://api.github.com/users/${user}/repos?per_page=${this.repos_count}&sort=${this.repos_sort}`, {
      headers: {
        Authorization: "Basic " + encoded64
      }
    });

    const profile = await profileResponse.json();
    const repos = await reposResponse.json();

    return {
      profile,
      repos
    }
  }
}