// Book constructor

function Book(title, author, isbn) {
  this.title = title;
  this.author = author;
  this.isbn = isbn;

}

// UI constructor
function UI() {

}
// Delete Book from ui
UI.prototype.deleteBook = function (target) {
  if (target.className === 'delete') {
    target.parentElement.parentElement.remove();
  }
}
// Show alert error
UI.prototype.showAllert = function (message, className) {
  // create element
  const div = document.createElement('div');
  // add classes
  div.className = `alert ${className}`;
  // add text
  div.appendChild(document.createTextNode(message));
  // get parent
  const container = document.querySelector('.container');
  const form = document.querySelector('#book-form');
  container.insertBefore(div, form);
  // timeout after 3 sec
  setTimeout(function () {
    document.querySelector('.alert').remove();
  }, 3000);
}
// Clear Fields
UI.prototype.clearFields = function () {
  document.getElementById('title').value = '';
  document.getElementById('author').value = '';
  document.getElementById('isbn').value = '';
}

UI.prototype.addBookToList = function (book) {
  const list = document.getElementById('book-list');
  // create element
  const row = document.createElement('tr');
  // insert cols
  row.innerHTML = `
    <td>${book.title}</td>
    <td>${book.author}</td>
    <td>${book.isbn}</td>
    <td><a href="#" class="delete">X</a></td>
  `;
  list.appendChild(row);
}

// Event listener for add book
document.getElementById('book-form').addEventListener('submit',
  function (e) {
    // Get form values
    const title = document.getElementById('title').value,
      author = document.getElementById('author').value,
      isbn = document.getElementById('isbn').value;
    // instance of book
    const book = new Book(title, author, isbn);
    // instantiate UI
    const ui = new UI();
    if (title === '' || author === '' || isbn === '') {
      // error alert show
      ui.showAllert('Please fill in all fields', 'error');
    } else {
      ui.addBookToList(book);
      // show success alert
      ui.showAllert('Book Added', 'success');
      ui.clearFields();
    }
    e.preventDefault();
  });

// Event listener for delete
document.getElementById('book-list').addEventListener('click',
  function (e) {
    const ui = new UI();

    ui.deleteBook(e.target);
    // show message
    ui.showAllert('Book deleted', 'success');
    e.preventDefault();
  });
