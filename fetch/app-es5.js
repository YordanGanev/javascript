document.getElementById('button1').addEventListener('click', getText);
document.getElementById('button2').addEventListener('click', getJSON);
document.getElementById('button3').addEventListener('click', getAPI);

// get local txt data
function getText() {
  fetch('text.txt')
    .then(res => res.text())
    .then(data => document.getElementById('output').innerHTML = data)
    .catch(err => console.log(err));
}

//get local json data
function getJSON() {
  fetch('posts.json')
    .then(res => res.json())
    .then(data => {
      console.log(data);
      let output = '';
      data.forEach(post => {
        output += `<li>${post.title}</li>`;
      });
      document.getElementById('output').innerHTML = output;
    })
    .catch(err => console.log(err));
}

// get external api data

function getAPI() {
  fetch('https://api.github.com/users')
    .then(function (res) {
      return res.json();
    })
    .then(function (data) {
      console.log(data);
      let output = '';
      data.forEach(user => {
        output += `<li>${user.login}</li>`;
      });
      document.getElementById('output').innerHTML = output;
    })
    .catch(function (err) {
      console.log(err);
    });
}

// arrow func
// const sayHello = function () {
//   console.log('Hello');
// }

// const sayHello = () => console.log('Hello');
// const sayHello = () => 'Hello';

// return Object literal
// const sayHello = () => ({ msg: 'Hello' });

// parameters 1 - name 2 - (name, other..)
// const sayHello = name => console.log(`hello ${name}`);

const users = ['nathan', 'john', 'william'];

// const nameLengths = users.map(function (name) {
//   return name.length;
// });

// shorter
const nameLengths = users.map(name => name.length);

console.log(nameLengths);